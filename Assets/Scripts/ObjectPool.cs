﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool instance;
    [System.Serializable]
    public struct PrefabPool{
        public GameObject prefab;
        public int amountInBuffer;

    };
    public PrefabPool[] prefabs;
    public Dictionary<string, Stack<GameObject>> generalPool = new Dictionary<string, Stack<GameObject>>();
    GameObject cointainerObject;
    void Awake(){
        instance = this;
        cointainerObject = new GameObject("ObjectPool");
        //generalPool = new List<GameObject>[prefabs.Length];
        foreach(PrefabPool objectPrefab in prefabs){
            generalPool[objectPrefab.prefab.name] = new Stack<GameObject>();
            for(int i = 0; i < objectPrefab.amountInBuffer; ++i){
                GameObject temp = Instantiate(objectPrefab.prefab);
                temp.name = objectPrefab.prefab.name;
                PoolGameObject(temp);
            }
        }
    }

    public void PoolGameObject(GameObject obj){
        obj.SetActive(false);
        generalPool[obj.name].Push(obj);
    }

    public GameObject GetGameObjectOfType(string objectType, bool onlyPooled){
        if (generalPool[objectType].Count > 0)
        {
            GameObject pooledObject = generalPool[objectType].Pop();

            pooledObject.transform.parent = null;
            pooledObject.SetActive(true);
            return pooledObject;
        }else if (!onlyPooled)
        {
            for (int i = 0; i < prefabs.Length; ++i)
            {
                GameObject prefab = prefabs[i].prefab;
                if (prefab.name == objectType)
                {
                    return Instantiate(prefab);
                }
            }
        }

        return null;
    }

    public Stack<GameObject> GetAllGameObjectsOfType(string objectType)
    {
        if (generalPool[objectType].Count > 0)
        {
            Stack<GameObject> pooledObjects = generalPool[objectType];
            foreach(GameObject pooledObject in pooledObjects)
            {
                pooledObject.transform.parent = null;
                pooledObject.SetActive(true);
            }

            generalPool[objectType] = new Stack<GameObject>();

            return pooledObjects;
        }

        return null;
    }

    public bool AllPooled(string objName)
    {
        foreach(PrefabPool prefabPool in prefabs)
        {
            if (prefabPool.prefab.name == objName)
            {
                if (generalPool[objName].Count == prefabPool.amountInBuffer)
                    return true;
                else
                    return false;
            }

        }
        return false;

    }
}
