﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.Random;
using System;
using Random = UnityEngine.Random;

public class FlyingAgent : MonoBehaviour
{
    [Header("Evasion movement settings")]
    public Vector3 scale;
    public Vector3 vel;
    [Header("Genotype settings")]
    public float ang = 0f, ang_rang = 35f, v_max = 5f, force = 10f, torque = 2f;
    public float life = 100f;
    public Vector2 battleFieldX;
    public Vector2 battleFieldY;
    public Vector2 battleFieldZ;
    [Header("Control Settings")]
    public float wih_median = 8f, wih_std_dev = 0f, who_median = 2f, who_std_dev = 1f, evasion;
    public int hnodes = 5, onodes = 2;
    [Header("Fixed Settings")]
    public AudioClip[] movingAudios;
    public AudioClip[] firingAudios;
    public ParticleSystem deathExplosion;
    public Material dissolveMaterial;
    public Material originalMaterial;
    public bool debug = false;
    public float distance_influence = 40f, k_rej = 1.25f, k_Att = 0.75f;
    public int Score { get { return score; } set { score=value; } }
    public FlyingAgentData AgentData
    {
        get
        {
            agentData.Score = this.Score;
            return agentData;
        }
        set
        {
            agentData = value;
            this.wih = agentData.Wih;
            this.who = agentData.Who;
            v = Random.Range(0, agentData.VMax);
            ang_rang = agentData.AngRang;
            force = agentData.Force;
            torque = agentData.Torque;
            life = agentData.Life;
            scale = agentData.Scale;
            vel = agentData.Vel;
            transform.rotation = Quaternion.identity;
            transform.Rotate(Vector3.up, ang);
            ang_rang /= 180f;
            rb.velocity = transform.forward * v;
            evasion = agentData.Evasion;
        }
    }

    private Vector3 Delta;
    private Vector3 pos;
    private int score = 0;
    private float v, d_player, originalHealth;
    private Transform playerPos;
    private AudioSource audioSource;
    private Matrix<float> wih, who;
    private Rigidbody rb;
    private float angle_front;
    private Vector2 front2d;
    private float nn_dv, nn_dr, shootRange, dissolveLevel;
    private int layer;
    private GameObject player;
    private Vector2 battleField;
    private bool evade, facing, onRange, onShootRange;
    private Automatic automaticGun;
    private LifeBarController lifeBar;
    private Renderer renderer;
    private bool isDead;
    private FlyingAgentData agentData;
    private HarmonicMovement hmEvasion, hmTeleport;
    private System.Random rand;
    Vector3 f_att = new Vector3(0f, 0f, 0f);
    Vector3 f_rej = new Vector3(0f,0f,0f);
    Vector3 a_to_a = new Vector3(0F, 0F, 0F);

    void Start()
    {
        renderer = GetComponentInChildren<Renderer>();
        dissolveLevel = dissolveMaterial.GetFloat("_Level");

        isDead = false;
        try
        {
            lifeBar = gameObject.GetComponentInChildren<LifeBarController>();
        }
        catch (System.Exception e)
        {
            Debug.Log(e, this);
        }
        //Physics.IgnoreLayerCollision(layer, layer);
        shootRange = automaticGun.Range/2f;
        SetInitialParameters();
        hmEvasion = new HarmonicMovement(scale, vel, 0);
        hmTeleport = new HarmonicMovement(scale, vel, 0);
        ThinkPotentialField();
        //Teletransport();
    }

    void Awake()
    {

        rb = GetComponent<Rigidbody>();
        player = GameObject.FindWithTag("PlayerTarget");
        layer = LayerMask.NameToLayer("Enemies");
        automaticGun = GetComponentInChildren<Automatic>();
        playerPos = player.transform;
        evade = false;

        if (debug)
        {
            Normal normalWIH = new Normal((float)wih_median, (float)wih_std_dev, new SystemRandomSource(true));
            Normal normalWHO = new Normal((float)who_median, (float)who_std_dev, new SystemRandomSource(true));
            wih = Matrix<float>.Build.Random(hnodes, onodes + 1, normalWIH);
            who = Matrix<float>.Build.Random(onodes, hnodes + 1, normalWHO);
            v = Random.Range(0, v_max);
            transform.Rotate(Vector3.up, ang);
            ang_rang /= 180f;
            rb.velocity = transform.forward * v;
        }
        battleField = new Vector2(battleFieldX.y - battleFieldX.x, battleFieldY.y - battleFieldY.x);
    }


    public void SetInitialParameters()
    {
        SetPosition();

        originalHealth = life;
        agentData.AliveTime = 0f;
        renderer.material = originalMaterial;
        isDead = false;
    }

    void SetPosition()
    {
        pos = new Vector3(Random.Range(battleFieldX.x, battleFieldX.y), Random.Range(battleFieldY.x, battleFieldY.y), Random.Range(battleFieldZ.x,battleFieldZ.y));
        transform.position = new Vector3(pos.x, pos.z, pos.y);
    }

    void DoEvassiveAction(System.Random seed)
    {
        if (seed.NextDouble() > evasion)
            Evade();
        else
            hmTeleport.SetPosition(this.gameObject);

    }

    // Fixed update for physics calculations
    void FixedUpdate()
    {
        if (!isDead)
        {
            agentData.AliveTime += Time.deltaTime / 60f;
            if (evade)
            {
                //DoEvassiveAction(rand);
                Evade();
                evade = false;
            }
            //else
            //    rand = new System.Random();
            //hmTeleport.SetHarmonicCurve();
            ThinkPotentialField();
            TurnPotentialField();
            facing = Facing(playerPos);
            if (!onRange)
            {
                automaticGun.Shooting = false;
                automaticGun.gameObject.GetComponentInChildren<ParticleSystem>().Stop();
                ThrustPotentialField();
            }
            else if (facing && onRange)
                Shoot();
        }


    }

    delegate Matrix<float> af(Matrix<float> input);
    void ThinkNN()
    {
        //Simple MLP
        //facing = Facing(playerPos);

        try
        {
            af activationFunction = x => {
                for (int i = 0; i < x.RowCount; i++)
                    for (int j = 0; j < x.ColumnCount; j++)
                        x[i, j] = (float)Trig.Tanh(x[i, j]); return x;
            }; //activation function
            Matrix<float> h1 = activationFunction(this.wih.Multiply(Vector<float>.Build.DenseOfArray(new float[] { (float)angle_front, (float)d_player, 1f })).ToColumnMatrix()); //hidden layer
            Matrix<float> output = activationFunction(this.who.Multiply(h1.Transpose().Append(Matrix<float>.Build.Dense(1, h1.ColumnCount, 1).Transpose()).Transpose())); //output layer
            nn_dv = (float)output[0, 0]; //[-1,1] (accelerate = 1, deaccelerate)
            nn_dr = (float)output[1, 0]; //[-1,1] (left = 1, right = -1)
        }catch(Exception e)
        {
            Debug.Log(e.Message);
        }



    }

    void TurnNNet()
    {
        rb.angularVelocity = transform.up * torque * nn_dr;
        //rb.AddTorque(transform.up * torque * nn_dv);
    }

    void ThrustNNtet()
    {
        rb.velocity = transform.forward * force * nn_dv;
        //rb.AddForce(transform.forward * force * nn_dv);

    }

    //Artificial potential field gradient calculation
    void ThinkPotentialField()
    {
        //Vector3 agentPos2d = new Vector3(transform.position.x, 0f, transform.position.z);
        //Vector3 playerPos2d = new Vector3(player.position.x, 0f, player.position.z);
        Vector3 agentPos2d = transform.position;
        Vector3 playerPos2d = playerPos.position;
        onRange = d_player <= automaticGun.Range / 3f;
        d_player = Vector3.Distance(agentPos2d, playerPos2d);
        f_rej = Vector3.zero;
        f_att = (agentPos2d - playerPos2d) / d_player;
        float d, r;
        foreach (Transform otherAgent in GameManager.Instance.otherAgents)
        {
            if (otherAgent != transform)
            {
                //d = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(otherAgent.position.x, otherAgent.position.z));
                d = Vector3.Distance(transform.position, otherAgent.position);
                //a_to_a = new Vector3(otherAgent.position.x - transform.position.x, 0f, otherAgent.position.z - transform.position.z) / d;
                a_to_a = otherAgent.position - transform.position;
                r = 1f / d - 1f / distance_influence;

                if (r >= 0f)
                    f_rej += k_rej * Mathf.Sqrt(r) * a_to_a;
            }

        }
    }

    void TurnPotentialField() => transform.forward = transform.forward - Time.deltaTime * (k_Att * f_att - f_rej);

    void ThrustPotentialField() => transform.position = transform.position - Time.deltaTime * (k_Att * f_att - f_rej);

    void Teletransport()
    {
        Vector3 aux = transform.position;
        if (aux.x > battleFieldX.y)
            aux.x = transform.position.x % (battleFieldX.y - battleFieldX.x);
        else if (aux.x < battleFieldX.x)
            aux.x = transform.position.x % (battleFieldX.y - battleFieldX.x) + (battleFieldX.y - battleFieldX.x);
        if (aux.z > battleFieldY.y)
            aux.z = transform.position.z % (battleFieldY.y - battleFieldY.x);
        else if (aux.z < battleFieldY.x)
            aux.z = transform.position.z % (battleFieldY.y - battleFieldY.x) + (battleFieldY.y - battleFieldY.x);
        transform.position = aux;
    }

    public void TakeDamage(float amount)
    {
        life -= amount;
        //Debug.Log("Enemy Health: " + life);
        lifeBar.SetLifeLevel(life / originalHealth);
        if (life <= 0f && !isDead)
        {
            DeadAction();
            GameManager.Instance.SetKills();
            isDead = true;
        }


    }

    public void SetDead(bool isDead) => this.isDead = isDead;

    public void DeadAction()
    {
        if (gameObject.activeSelf)
        {
            deathExplosion.Play();
            renderer.material = dissolveMaterial;
            renderer.material.SetFloat("_Level", dissolveLevel);
            InvokeRepeating("Dissolve", 0f, 0.1f);
            StartCoroutine(WaitToDie());
            
        }
    }

    IEnumerator WaitToDie()
    {
        yield return new WaitForSecondsRealtime(deathExplosion.main.duration * 2f);        
        CancelInvoke("Dissolve");
        StopCoroutine(WaitToDie());
        ObjectPool.instance.PoolGameObject(gameObject);
        GameManager.Instance.otherAgents.Remove(this.gameObject.transform);
    }

    void Dissolve()
    {
        if(renderer.material == dissolveMaterial)
        {
            float aux = renderer.material.GetFloat("_Level") - Time.deltaTime;
            aux = Mathf.Clamp(aux, 0f, 1f);
            renderer.material.SetFloat("_Level", aux);
        }

    }

    //private void PlaySounds(AudioClip[] audioClips)
    //{
    //    try
    //    {
    //        // pick & play a random sound from the array,
    //        // excluding sound at index 0
    //        int n = UnityEngine.Random.Range(1, audioClips.Length);
    //        source.clip = audioClips[n];
    //        source.PlayOneShot(source.clip, 1f);
    //        // move picked sound to index 0 so it's not picked next time
    //        audioClips[n] = audioClips[0];
    //        audioClips[0] = source.clip;
    //    }
    //    catch (System.Exception e)
    //    {
    //        Debug.LogException(e, this);
    //    }

    //}

    void Shoot()
    {
        automaticGun.gameObject.transform.LookAt(player.transform.position + new Vector3(Random.Range(-30f, 30f), Random.Range(-5f, 5f), Random.Range(-20f, 20f)));
        automaticGun.Shooting = true;
    }

    void Evade()
    {
        hmEvasion.SetHarmonicCurve();
        hmEvasion.SetPosition(this.gameObject);
    }

    bool Facing(Transform player)
    {
        d_player = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(player.position.x, player.position.z));
        onRange = d_player <= automaticGun.Range / 3.5f;

        d_player = d_player/battleField.magnitude;
        Vector2 agentPos2d = new Vector2(transform.position.x, transform.position.z);
        Vector2 playerPos2d = new Vector2(player.position.x, player.position.z);
        Vector2 vec = playerPos2d - agentPos2d;
        float angleBack;
        Vector2 back2d;
        vec.Normalize();
        front2d = new Vector2(transform.forward.x, transform.forward.z).normalized;
        back2d = new Vector2(-transform.forward.x, -transform.forward.z).normalized;
        angleBack = Vector2.SignedAngle(back2d, vec);
        float phi = Mathf.Atan2(vec.y, vec.x) * Mathf.Rad2Deg;
        angle_front = (Mathf.Atan2(front2d.y, front2d.x) * Mathf.Rad2Deg - phi);
        if (angle_front < -180f)
            angle_front += 360f;
        else if (angle_front > 180f)
            angle_front -= 360f;
        angle_front /= 180f;
        angleBack /= 180f;
        return angle_front <= ang_rang && angle_front >= -ang_rang;/*|| (angleBack <= ang_rang && angleBack >= -ang_rang)*/
    }

    public void SetEvade() => evade = true;
}
