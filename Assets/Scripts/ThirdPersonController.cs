﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonController : MonoBehaviour
{
    public float height, distance, smooth = 0.3f, rotSpeed = 0.5f, minimumDistance = 0.5f, maximumDistance = 100f;
    private Transform target;
    private GameObject targetObject, playerObject;
    private PlayerController player;
    private float yVelocity, yaw, pitch;
    private bool shootState, aimingMode = false;
    // Start is called before the first frame update
    void Start()
    {
        targetObject = GameObject.FindGameObjectWithTag("CannonTarget");
        playerObject = GameObject.FindGameObjectWithTag("PlayerTarget");
        player = targetObject.GetComponentInParent<PlayerController>();
        target = playerObject.transform;
        shootState = false;
        yaw = Mathf.SmoothDampAngle(transform.eulerAngles.y, target.eulerAngles.y, ref yVelocity, smooth);
    }

    // Late update for camera position update
    void LateUpdate()
    {
        Vector3 position;

        if (player.aimingMode){
            target = targetObject.transform;
            position = target.position;
            yaw += player.Yaw;
            yaw = Mathf.SmoothDampAngle(transform.eulerAngles.y, target.eulerAngles.y, ref yVelocity, smooth);
            position += Quaternion.Euler(Mathf.Clamp(player.Cannon.localEulerAngles.x - player.angMin, -player.angMin, player.angMax), yaw, 0) * new Vector3(0, height, -20f);
            shootState = true;
        }
        else{

            target = playerObject.transform;
            position = target.position;
            if (shootState)
            {
                yaw = player.Yaw + 180f;
                pitch = player.Pitch;
                shootState = false;
            }
            else
            {
                yaw += 2f * Input.GetAxis("Mouse X") * rotSpeed * Time.fixedDeltaTime;
                pitch += Input.GetAxis("Mouse Y") * rotSpeed * Time.fixedDeltaTime;
                position += Quaternion.Euler(Mathf.Clamp(pitch, -player.angMax, player.angMin-1), yaw, 0) * new Vector3(0, 0, -distance);
            }



        }
        transform.position = position;
        transform.LookAt(target);
        float d = Vector3.Distance(transform.position, target.position);
        distance -= Input.mouseScrollDelta.y;
        distance = Mathf.Clamp(distance, minimumDistance, maximumDistance);

        
    }


}
