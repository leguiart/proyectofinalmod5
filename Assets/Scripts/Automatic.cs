﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Automatic : Gun
{
    public AutoGun autoGun;
    public bool multiplePs;
    // Start is called before the first frame update
    void Awake()
    {
        this.Initialize(multiplePs);
        delay = 1f / delayRate;
        damage = autoGun.damage;
        range = autoGun.range;
        fireRate = autoGun.fireRate;
        impactForce = autoGun.impactForce;
        impactClip = autoGun.impactClip;
        shootSounds = autoGun.shootSounds;
        impactEffect = autoGun.impactEffect;
        impactVolume = autoGun.impactVolume;
        ImpactDuration = autoGun.impactDuration;
        primary = autoGun.primary;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (shooting && Time.time >= nextTimeToFire)
        {

            PlayShootSound();
            nextTimeToFire = Time.time + 1f / fireRate;
            delay = Time.time + delayRate;
            base.Shoot();

        }
        else if (Time.time >= delay)
            source.Stop();
        this.RaycastHit();
    }
}
