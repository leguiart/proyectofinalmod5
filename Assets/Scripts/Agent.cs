﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum AgentState {PATROL, CHASE, IDLE};

public class Agent : MonoBehaviour
{
    public string triggerColliderTag;
    public float health = 50f, hitDamage = 10f, updatePatrolPosition = 2.0f, detectionRadius = 4f;
    public bool detectWithTrigger = true;
    public AudioClip[] m_screams, m_dies, m_grunts, m_attacks, m_bites;
    public Vector2 patrolRange;
    public ParticleSystem ps;

    private Transform playerTransform;
    private EnemiesEvent enemyEvent;
    private NavMeshAgent agent;
    private Animator animator;
    private AudioSource source;
    private new Collider collider;
    //private PlayerDamage playerD;
    private PlayerController playerC;
    private bool alive, seeing;
    private float speed, totalHealth;
    private Vector3 target;
    private AgentState currentState;
    private RaycastHit visionHit;
    private LifeBarController lifeBar;
    private GameManager GameManager;

    void Start()
    {
        alive = true; seeing = false;
        GameObject GameManagerObject = GameObject.FindWithTag("GameManager");
        if(GameManagerObject != null)
            GameManager = GameManagerObject.GetComponent<GameManager>();
        else
            Debug.Log("Can't find game controller object");

        GameObject player = GameObject.FindWithTag("Player");
        if (detectWithTrigger)
        {
            GameObject trigger = GameObject.FindWithTag(triggerColliderTag);
            currentState = AgentState.IDLE;
            if (trigger != null)
                enemyEvent = trigger.GetComponent<EnemiesEvent>();
            else
                Debug.Log("Can't find triggerCollider object");
        }
        else
        {
            currentState = AgentState.PATROL;
            //enemyEvent = GetComponentInChildren<EnemiesEvent>();
            InvokeRepeating("CalculateDestination", 0f, updatePatrolPosition);
        }

        if (player != null)
        {
            playerTransform = player.GetComponent<Transform>();
            try
            {
                //playerD = player.GetComponent<PlayerDamage>();
                Debug.Log("Got Player Damge component");
            }catch (Exception)
            {
                playerC = player.GetComponent<PlayerController>();
                Debug.Log("Got Player Controller component");
            }
            
        }
        else
        {
            Debug.Log("Can't find RigidbodyFirstPersonController.cs script");
        }

        try
        {
            agent = GetComponent<NavMeshAgent>();
            animator = GetComponent<Animator>();
            collider = GetComponent<Collider>();
            source = GetComponent<AudioSource>();
            lifeBar = gameObject.GetComponentInChildren<LifeBarController>();
            ps = GetComponentInChildren<ParticleSystem>();
        }
        catch(Exception e){
            Debug.Log(e, this);
        }

    }

    private void Awake()
    {
        totalHealth = health;
    }

    private void Update()
    {

        if(this.enemyEvent != null)
        {
            if (this.enemyEvent.GetEnemiesStatus())
                currentState = AgentState.CHASE;
        }
        //Si el Trigger collider asociado al agente ha detectado una collisión con el jugador o la distancia al jugador es la mínima para que se active, y el zombie está vivo
        if (currentState == AgentState.CHASE && alive)
        {
            //Establecer el destino del NavMeshAgent a la posición del jugador
            this.agent.SetDestination(playerTransform.position);
            //Si la distancia euclideana entre el jugador y el agente es menor o igual que la distancia a la cuál para el NavMeshAgent 
            if (Vector3.Distance(this.agent.destination, this.agent.transform.position) <= this.agent.stoppingDistance)
            {

                FaceTarget();//Hacer que el zombie siempre se encuentre viendo hacia el jugador, por medio de la obtención del segmento dirigido normalizado entre el zombie y el jugador
                //y rotar en esa dirección proyectada en x y z de una forma suave por medio de interpolación lineal esférica
                ps.Play();
                RaycastHit visionHit;
                if (Physics.SphereCast(gameObject.transform.position, detectionRadius, gameObject.transform.forward, out visionHit))
                {
                    //Debug.Log("Vision Hit: " + visionHit.transform.name);
                    if (visionHit.transform.tag == "Player")
                        visionHit.transform.gameObject.GetComponent<PlayerController>().TakeDamage(hitDamage);
                }
            }
 
        } else if(currentState == AgentState.PATROL && alive)
        {
            this.agent.SetDestination(target);
            //animator.SetBool("isAttacking", false);

            //if (source != null && !source.isPlaying)
            //    PlaySounds(m_grunts);
        }     
        //Pasarle la velocidad del NavMeshAgent al blendtree para crear un ciclo de caminado acorde 
        speed = agent.velocity.magnitude;
        animator.SetFloat("Speed", speed);

    }

    private void PlaySounds(AudioClip[] audioClips)
        {
            try
            {
                // pick & play a random sound from the array,
                // excluding sound at index 0
                int n = UnityEngine.Random.Range(1, audioClips.Length);
                source.clip = audioClips[n];
                source.PlayOneShot(source.clip, 1f);
                // move picked sound to index 0 so it's not picked next time
                audioClips[n] = audioClips[0];
                audioClips[0] = source.clip;
            }
            catch (Exception e)
            {
                Debug.LogException(e, this);
            }

        }


    private void FaceTarget()
    {
        Vector3 direction = (playerTransform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    void Die()
    {
        animator.SetBool("isDead", true);
        agent.enabled = false;
        collider.enabled = false;
        alive = false;
        GameManager.SetKills();
        PlaySounds(m_dies);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
    }

    void CalculateDestination()
    {
        target = transform.position + new Vector3(UnityEngine.Random.Range(patrolRange.x, patrolRange.y), 0, UnityEngine.Random.Range(patrolRange.x, patrolRange.y));
    }

    void DetectPlayer()
    {
        if (seeing)
        {
            currentState = AgentState.CHASE;
            CancelInvoke("CalculateDestination");
            //Debug.Log("Detectando");
        }


    }


    public float GetSpeed()
    {
        return speed;
    }

    public void TakeDamage(float amount)
    {
        health -= amount;
        Debug.Log("Enemy Health: " + health);
        lifeBar.SetLifeLevel(health / totalHealth);
        if (health <= 0f)
            Die();

    }

    //public void MakeDamage()
    //{
    //    //Si se encuentra en las animaciones de Attack1 o Attack2 y el tiempo que ha pasado desde el último hit es mayor al del próximo hit
    //    if ((animator.GetBehaviour<StateBehaviour2>().GetOnState() || animator.GetBehaviour<StateBehaviour3>().GetOnState()))
    //    {
    //        //nexTime = Time.time + 1f / hitRate;//Aumenta el tiempo hasta el próximo hit
    //        if (playerD != null)
    //            playerD.TakeDamage(hitDamage);
    //        else
    //            playerC.TakeDamage(hitDamage);//Inflingir daño en el jugador
    //    }
    //}
}
