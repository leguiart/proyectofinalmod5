﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelMover : MonoBehaviour
{
    public float force;
    public Vector2 battleFieldX;
    public Vector2 battleFieldY;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * force, ForceMode.Acceleration);
    }

    // Update is called once per frame
    void Update()
    {
        Teletransport();
    }

    void Teletransport()
    {
        Vector3 aux = transform.position;
        if (aux.x > battleFieldX.y)
            aux.x = transform.position.x % (battleFieldX.y - battleFieldX.x);
        else if (aux.x < battleFieldX.x)
            aux.x = transform.position.x % (battleFieldX.y - battleFieldX.x) + (battleFieldX.y - battleFieldX.x);
        if (aux.z > battleFieldY.y)
            aux.z = transform.position.z % (battleFieldY.y - battleFieldY.x);
        else if (aux.z < battleFieldY.x)
            aux.z = transform.position.z % (battleFieldY.y - battleFieldY.x) + (battleFieldY.y - battleFieldY.x);
        transform.position = aux;
    }
}
