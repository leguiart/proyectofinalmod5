﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBarController : MonoBehaviour
{

    public Color fullColor;
    public Color emptyColor;
    private Image lifeBar;

    void Awake()
    {
        lifeBar = GetComponent<Image>();
    }

    private void Update()
    {
        if(gameObject.GetComponentInParent<FlyingAgent>() != null)
            transform.LookAt(Camera.main.transform);
    }

    public void SetLifeLevel(float lifeLevel)
    {
        try
        {
            lifeBar.color = Color.Lerp(emptyColor, fullColor, lifeLevel);
            lifeBar.fillAmount = lifeLevel;
        }
        catch(Exception e)
        {
            Debug.Log(e);
        }
        

    }
}
