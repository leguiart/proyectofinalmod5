﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
            transform.parent.SendMessage("DetectPlayer", SendMessageOptions.DontRequireReceiver);
    }
}
