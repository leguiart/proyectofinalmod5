﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; }

    public GameObject HUDPanel, pausePanel, lifeBar, loadingBar, restartPanel, loadingPanel;
    public Slider musicSlider, sfxSlider;
    public TextMeshProUGUI gameOverText, restartText, livesText, itemsText;
    private LifeBarController lifeBarController, loadingBarController;
    private AsyncOperation sceneLoader;

    bool loading;

    void Awake(){
        Instance = this;
        loading = false;
        if (lifeBar != null)
            lifeBarController = lifeBar.GetComponent<LifeBarController>();
        else
            Debug.Log("Can't find lifebar controller");

        if (loadingBar != null)
            loadingBarController = loadingBar.GetComponent<LifeBarController>();
        else
            Debug.Log("Can't find lifebar controller");
    }

    void Start()
    {
        CleanPanels();
        ShowHUD();
        restartText.text = "";
        gameOverText.text = "";

        //ResetLifeBar();
    }

    void CleanPanels(){
        HUDPanel.SetActive(false);
        pausePanel.SetActive(false);
        restartPanel.SetActive(false);
        loadingPanel.SetActive(false);
    }

    void Update()
    {
        if (loadingPanel.activeSelf)
        {
            if (loading)
                loadingBarController.SetLifeLevel(sceneLoader.progress);
            else
                loadingBarController.SetLifeLevel(0f);
        }


    }

    public void Restart()
    {
        Time.timeScale = 1f;
        loading = true;
        sceneLoader = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

    public void ShowHUD(){
        CleanPanels();

        HUDPanel.SetActive(true);
    }

    public void HidePause()
    {
        CleanPanels();
        HUDPanel.SetActive(true);
    }

    public void ShowPause(){
        CleanPanels();
        pausePanel.SetActive(true);
    }

    public void SetLives(int lives){
        livesText.text = "x " + lives.ToString();
    }

    public void SetKills(int kills){
        itemsText.text = "x " + kills.ToString();
    }

    public void SetRestartText(string text)
    {
        restartText.text = text;
    }

    public void SetRestartPanel()
    {
        CleanPanels();
        restartPanel.SetActive(true);
    }

    public void SetGameOverText(string text)
    {
        gameOverText.text = text;
    }

    public void SetMusicVolume(float volume)
    {
        musicSlider.value = volume;
    }

    public void GetMusicVolume(float sValue){
        AudioManager.Instance.SetMusicVolume(sValue);
    }

    public void SetSFXVolume(float volume){
        sfxSlider.value = volume;
    }

    public void GetSFXVolume(float sValue){
        AudioManager.Instance.SetSFXVolume(sValue);
    }
    public void ExitGame(){
        DataManager.Instance.SavePlayerData(GameManager.Instance.Player);
        DataManager.Instance.SetFlyingAgentsData(GameManager.Instance.FlyingAgents);
        loading = true;
        
        sceneLoader = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex - 1, LoadSceneMode.Single);
    }
    public void SetHealthValue(float normalizedHealthValue)
    {
        lifeBarController.SetLifeLevel(normalizedHealthValue);
    }
}
