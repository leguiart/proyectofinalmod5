﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class DissolveEffect : MonoBehaviour
{

    public Shader dissolveShader;

    public Color mainColor = Color.white;
    public float dissolutionLevel;
    public Color edgeColor1 = Color.white;
    public Color edgeColor2 = Color.black;
    public float edgeWidth = 0.5f;
    public float smoothness = 0.65f;
    public float metallic = 0.25f;

    public Texture2D albedoTexture;
    public Texture2D dissolveTexture;
    private Material currentMaterial;

    Material material
    {
        get
        {
            if (currentMaterial == null)
            {
                currentMaterial = new Material(dissolveShader);
                currentMaterial.hideFlags = HideFlags.HideAndDontSave;
            }
            return currentMaterial;
        }

    }

    private void Start()
    {
        if (!SystemInfo.supportsImageEffects)
        {
            enabled = false;
            return;
        }
        if (!dissolveShader && !dissolveShader.isSupported)
            enabled = false;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (dissolveShader != null)
        {
            material.SetColor("_Color", mainColor);
            material.SetFloat("_Level", dissolutionLevel);
            material.SetColor("_EdgeColour1", edgeColor1);
            material.SetColor("_EdgeColour2", edgeColor2);
            material.SetFloat("_Edges", edgeWidth);
            material.SetFloat("_Glossiness", smoothness);
            material.SetFloat("_Metallic", metallic);

            if (dissolveTexture)
            {
                material.SetTexture("_DissolveTex", dissolveTexture);
            }
            if (albedoTexture)
            {
                material.SetTexture("_MainTex", albedoTexture);
            }
            Graphics.Blit(source, destination, material);
        }
        else
        {
            Graphics.Blit(source, destination);
        }
    }

    public void Update()
    {
        dissolutionLevel = Mathf.Clamp(dissolutionLevel, 0f, 1f);
        edgeWidth = Mathf.Clamp(edgeWidth, 0f, 1f);
        smoothness = Mathf.Clamp(smoothness, 0f, 1f);
        metallic = Mathf.Clamp(metallic, 0f, 1f);
    }

    private void OnDisable()
    {
        if (currentMaterial)
        {
            DestroyImmediate(currentMaterial);
        }
    }
}
