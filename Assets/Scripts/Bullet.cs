﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [HideInInspector] public GameObject impactEffect;
    [HideInInspector] public float damage, impactDuration, impactVolume;
    [HideInInspector] public AudioClip impactClip;
    private Agent agent;

    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint contact = collision.contacts[0];
        GameObject impact = Instantiate(impactEffect, contact.point, Quaternion.FromToRotation(Vector3.up, contact.normal));
        Agent agent = collision.transform.GetComponent<Agent>();
        FlyingAgent flyingAgent = collision.transform.GetComponent<FlyingAgent>();
        if (agent != null)
            agent.TakeDamage(damage);
        
        if (flyingAgent != null)
            flyingAgent.TakeDamage(damage);

        //AudioSource.PlayClipAtPoint(impactClip, contact.point, impactVolume);
        Destroy(this.gameObject, impactDuration);
        Destroy(impact, impactDuration);
    }
}
