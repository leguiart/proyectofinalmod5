﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform Cannon { get; private set; }

    public float speed = 0.5f, speedRotation = 2f, rotSpeed = 0.5f, angMax = 45f, angMin = 0f;
    public GameObject respawnPoint;
    public bool aimingMode = false;
    private Transform hull;
    private Manual cannonGun;
    private Automatic gatlingGun;
    private Rigidbody rb;
    private bool respawning;
    private float yaw = 0f, pitch = 0f, rightOffset = 0f;
    // Start is called before the first frame update
    void Start()
    {
        SetMouseState(false, CursorLockMode.Locked);
        //PlayAnim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        hull = GameObject.Find("Hull").transform;
        Cannon = GameObject.Find("Cannon").transform;
        cannonGun  = GetComponentInChildren<Manual>();
        gatlingGun  = GetComponentInChildren<Automatic>();
        respawning = false;
        //GameManager.Instance.SetLives();
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(2) && !aimingMode)
            aimingMode = true;
        else if (Input.GetMouseButtonDown(2) && aimingMode)
            aimingMode = false;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //PlayAnim.SetFloat("Speed", Input.GetAxis("Vertical"));
        if (!respawning)
        {

            if (aimingMode)
            {
                //if (Input.GetKey(KeyCode.))
                //{
                //    rightOffset += 0.5f;
                //    rb.velocity = transform.right * speed * rightOffset * Time.deltaTime;
                //}
                //else if (Input.GetKey(KeyCode.Q))
                //{
                //    rightOffset -= 0.5f;
                //    rb.velocity = transform.right * speed * rightOffset * Time.deltaTime;
                //}
                //else
                //{
                //    rightOffset = 0f;
                //}
                    
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    rb.velocity = 4f * transform.forward * speed / 2f * Input.GetAxis("Vertical") + 4f * transform.right * speed * Input.GetAxis("Horizontal");
                }
                else
                {
                    rb.velocity = transform.forward * speed /2f * Input.GetAxis("Vertical") + transform.right * speed * Input.GetAxis("Horizontal");
                }
                yaw += Input.GetAxis("Mouse X") * rotSpeed * Time.fixedDeltaTime;
                transform.Rotate(new Vector3(0f, yaw, 0f), Space.World);
                pitch += Input.GetAxis("Mouse Y") * rotSpeed * Time.fixedDeltaTime;

                Cannon.Rotate(new Vector3(pitch, 0f, 0f), Space.Self);
                Cannon.transform.localEulerAngles = new Vector3(Mathf.Clamp(Cannon.transform.localEulerAngles.x, angMax, angMin - 2f), 0f, 0f);
            }
            else
            {
                if (Input.GetKey(KeyCode.LeftShift))
                    rb.velocity = 4f * transform.forward * speed * Input.GetAxis("Vertical");
                else
                    rb.velocity = transform.forward * speed * Input.GetAxis("Vertical");
                rb.angularVelocity = speedRotation * transform.up * Input.GetAxis("Horizontal");
                yaw = 0f;
                pitch = 0f;
                hull.rotation = transform.rotation;
                hull.Rotate(new Vector3(-90f, 0f, 0f), Space.Self);
                Cannon.rotation = transform.rotation;

            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                //PlayAnim.SetTrigger("Shoot");
                cannonGun.Shooting = true;
            }
            else
            {
                cannonGun.Shooting = false;
            }

            if (Mathf.Round(Input.GetAxisRaw("Fire1")) == 1)
            {
                gatlingGun.Shooting = true;
            }
            else
            {
                gatlingGun.Shooting = false;
            }
        }

        //if(PlayAnim.GetBehaviour<StateBehaviour6>().OnState)
            //health = originalHealth;
    }
    public float Yaw {get{return yaw;} set{yaw = value;}}
    public float Pitch{get{return pitch;} set{pitch = value;}}

    public void SetMouseState(bool visibility, CursorLockMode lockMode)
    {
        Cursor.visible = visibility;
        Cursor.lockState = lockMode;
    }

    public void TakeDamage(float hitDamage)
    {
        GameManager.Instance.SetHealthValue(hitDamage);
        if (GameManager.Instance.Player.health <= 0f)
            Die();

    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.transform.name == "BiteCollider" || other.transform.name == "LeftHandCollider" || other.transform.name == "RightHandCollider" || other.transform.name == "LeftFootCollider" || other.transform.name == "RightFootCollider")
        //    other.gameObject.GetComponentInParent<Agent>().MakeDamage();
        if(other.transform.tag == "Price")
        {
            GameManager.Instance.RewardLives();
            other.gameObject.SetActive(false);
        }
    }

    void Die()
    {
        //PlayAnim.SetBool("isDead", true);
        gameObject.GetComponentInChildren<Collider>().enabled = false;
        gameObject.GetComponent<Rigidbody>().useGravity = false;
        rb.velocity = new Vector3(0f, 0f, 0f);
        rb.angularVelocity = new Vector3(0f, 0f, 0f);
        GameManager.Instance.SetLives();
        if (GameManager.Instance.Player.lives > 0){        
            StartCoroutine(Respawn());
        }else{
            GameManager.Instance.UpdateGameOver();
            Time.timeScale = 0.3f;
        }
            
    }

    IEnumerator Respawn(){
        respawning = true;
        yield return new WaitForSeconds(10f);
        gameObject.transform.position = respawnPoint.transform.position;
        gameObject.transform.rotation = respawnPoint.transform.rotation;
        GameManager.Instance.ResetHealth();
        //PlayAnim.SetBool("isDead", false);
        yield return new WaitForSeconds(3f);
        gameObject.GetComponentInChildren<Collider>().enabled = true;
        gameObject.GetComponent<Rigidbody>().useGravity = true;
        respawning = false;
        StopCoroutine(Respawn());

    }


}
