﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace Data
{
    [Serializable]
    public class Player
    {
        public int lives;
        public int kills;
        public float sfxVolume;
        public float musicVolume;
        public float health;
        public float originalHealth;
        public int waveNum;
        public int level;
    }
}

