﻿namespace Data
{
    using System.Collections.Generic;
    using UnityEngine;
    using MathNet.Numerics.Distributions;
    using MathNet.Numerics.LinearAlgebra;

    [System.Serializable]
    public class FlyingAgentsData
    {
        public FlyingAgentsData(Dictionary<string, object> settings)
        {
            Settings = settings;
            Population = new List<FlyingAgentData>();
        }

        public FlyingAgentsData()
        {

        }

        public void Populate()
        {
            Normal normalWIH = new Normal((float)Settings["wih_median"], (float)Settings["wih_std_dev"]);
            Normal normalWHO = new Normal((float)Settings["who_median"], (float)Settings["who_std_dev"]);
            for (int i = 0; i < (int)Settings["pop_size"]; i++)
            {
                Population.Add(new FlyingAgentData(new Dictionary<string, object> {
                    {"ang",  (float)Normal.Sample((float)Settings["ang_median"], (float)Settings["ang_std_dev"])},
                    {"ang_rang", (float)Normal.Sample((float)Settings["ang_rang_median"], (float)Settings["ang_rang_std_dev"]) },
                    {"v_max", (float)Settings["v_max"]},                   
                    {"force",  (float)Normal.Sample((float)Settings["force_median"], (float)Settings["force_std_dev"])},
                    {"torque",  (float)Normal.Sample((float)Settings["torque_median"], (float)Settings["torque_std_dev"])},
                    {"wih", Matrix<float>.Build.Random((int)Settings["hnodes"], (int)Settings["onodes"] + 1, normalWIH) },
                    {"who", Matrix<float>.Build.Random((int)Settings["onodes"], (int)Settings["hnodes"] + 1, normalWIH) },
                    {"scale", new Vector3((float)Normal.Sample((float)((Vector3)Settings["scale_median"]).x, (float)((Vector3)Settings["scale_std_dev"]).x),
                                          (float)Normal.Sample((float)((Vector3)Settings["scale_median"]).y, (float)((Vector3)Settings["scale_std_dev"]).y),
                                          (float)Normal.Sample((float)((Vector3)Settings["scale_median"]).z, (float)((Vector3)Settings["scale_std_dev"]).z))},
                    { "vel", new Vector3((float)Normal.Sample((float)((Vector3)Settings["vel_median"]).x, (float)((Vector3)Settings["vel_std_dev"]).x),
                                          (float)Normal.Sample((float)((Vector3)Settings["vel_median"]).y, (float)((Vector3)Settings["vel_std_dev"]).y),
                                          (float)Normal.Sample((float)((Vector3)Settings["vel_median"]).z, (float)((Vector3)Settings["vel_std_dev"]).z))},
                    {"scale2", new Vector3((float)Normal.Sample((float)((Vector3)Settings["scale_median"]).x, (float)((Vector3)Settings["scale_std_dev"]).x),
                                          (float)Normal.Sample((float)((Vector3)Settings["scale_median"]).y, (float)((Vector3)Settings["scale_std_dev"]).y),
                                          (float)Normal.Sample((float)((Vector3)Settings["scale_median"]).z, (float)((Vector3)Settings["scale_std_dev"]).z))},
                    { "vel2", new Vector3((float)Normal.Sample((float)((Vector3)Settings["vel_median"]).x, (float)((Vector3)Settings["vel_std_dev"]).x),
                                          (float)Normal.Sample((float)((Vector3)Settings["vel_median"]).y, (float)((Vector3)Settings["vel_std_dev"]).y),
                                          (float)Normal.Sample((float)((Vector3)Settings["vel_median"]).z, (float)((Vector3)Settings["vel_std_dev"]).z))},
                    {"life", (float)Settings["life"] },
                    {"evasion_threshold", Random.Range(0f, 1f) }
                }));
            }
        }

        delegate bool del(Vector<float> row);
        public void Evolve(int gen)
        {
            int elitism_num = (int)Mathf.Floor((float)Settings["elitism"] * (int)Settings["pop_size"]);
            int new_orgs = (int)Settings["pop_size"];
            /*
             * Selection:
             * - Fitness function evaluation
             * - Actual selection method
             * - Apply elitism (or not)
             */
            //Get stats from current generation
            float sum = 0, avg, best = 0, worst = 0;
            int count = 0;
            foreach(FlyingAgentData org in Population)
            {

                sum += (float)org.Score;
                count++;
            }
            avg = sum / count;

            foreach(FlyingAgentData org in Population)
            {
                org.Fitness = org.Score / (avg * (GenerationTime - org.AliveTime) + avg);
                if (org.Fitness > best || best == 0)
                    best = org.Fitness;
                if (org.Fitness < worst || worst == 0)
                    worst = org.Fitness;
            }

            //Elitism: Keep Best performing organisms
            List<FlyingAgentData> sortedPopulation = new List<FlyingAgentData>(Population);
            sortedPopulation.Sort((x, y) => x.Fitness.CompareTo(y.Fitness));
            sortedPopulation.Reverse();
            Population = new List<FlyingAgentData>();
            for(int i = 0; i < elitism_num; ++i)
            {
                sortedPopulation[i].Score = 0;
                Population.Add(sortedPopulation[i]);
            }



            //Reproduccion (Generacion de nuevos individuos)
            for(int i = elitism_num; i < (int)Settings["pop_size"]; i++)
            {
                Dictionary<string, object> AuxDict = new Dictionary<string, object>();
                //Seleccion (Trunc selection)
                System.Random rand = new System.Random();
                int[] random_index = new int[2];
                random_index[0] = rand.Next(0, elitism_num);
                random_index[1] = rand.Next(0, elitism_num);
                FlyingAgentData org_1 = sortedPopulation[random_index[0]];
                FlyingAgentData org_2 = sortedPopulation[random_index[1]];
                //Crossover
                //Hay que normalizar algunos parametros
                float crossover_weight = 1f - (float)rand.NextDouble()*0.5f;
                AuxDict.Add("wih", crossover_weight * org_1.Wih + (1f - crossover_weight * org_2.Wih));
                AuxDict.Add("who", crossover_weight * org_1.Who + (1f - crossover_weight * org_2.Who));
                AuxDict.Add("v_max", crossover_weight * org_1.VMax + (1f - crossover_weight * org_2.VMax));
                AuxDict.Add("ang_rang", crossover_weight * org_1.AngRang + (1f - crossover_weight * org_2.AngRang));
                AuxDict.Add("force", crossover_weight * org_1.Force + (1f - crossover_weight * org_2.Force));//en especial esto
                AuxDict.Add("torque", crossover_weight * org_1.Torque + (1f - crossover_weight * org_2.Torque));
                AuxDict.Add("vel", crossover_weight * org_1.Vel + (new Vector3(1f,1f,1f) - crossover_weight * org_2.Vel));
                AuxDict.Add("scale", crossover_weight * org_1.Scale + (new Vector3(1f, 1f, 1f) - crossover_weight * org_2.Scale));
                AuxDict.Add("vel2", crossover_weight * org_1.Vel2 + (new Vector3(1f, 1f, 1f) - crossover_weight * org_2.Vel2));
                AuxDict.Add("scale2", crossover_weight * org_1.Scale + (new Vector3(1f, 1f, 1f) - crossover_weight * org_2.Scale));
                AuxDict.Add("life", (float)Settings["life"] + (float)gen*10f);
                AuxDict.Add("score", 0F);
                AuxDict.Add("evasion_threshold", (org_1.Evasion + org_2.Evasion) / 2f);
                AuxDict.Add("ang", (float)Normal.Sample((float)Settings["ang_median"], (float)Settings["ang_std_dev"]));
                //Mutacion
                float mutate = Random.Range(0f, 1f);
                if(mutate <= (float)Settings["mutate"])
                {
                    //Elegir matriz de pesos a mutar
                    int mat_pick = rand.Next(0, 1);
                    del anyG = x => {
                        for (int j = 0; j < x.Count; ++j)
                        {
                            if (x[j] > 1f)
                                return true;
                        }
                        return false;
                    };
                    del anyL = x => {
                        for (int j = 0; j < x.Count; ++j)
                        {
                            if (x[j] < 1f)
                                return true;
                        } return false;
                    };

                    if (mat_pick == 0)
                    {
                        int index_row = rand.Next(0, (int)Settings["hnodes"] - 1);
                        ((Matrix<float>)AuxDict["wih"]).SetRow(index_row, ((Matrix<float>)AuxDict["wih"]).Row(index_row)*Random.Range(0.9f, 1.1f));
                        if (anyG(((Matrix<float>)AuxDict["wih"]).Row(index_row)))
                            ((Matrix<float>)AuxDict["wih"]).SetRow(index_row, Vector<float>.Build.Dense(((Matrix<float>)AuxDict["wih"]).ColumnCount, 1f));
                        else if(anyL(((Matrix<float>)AuxDict["wih"]).Row(index_row)))
                            ((Matrix<float>)AuxDict["wih"]).SetRow(index_row, Vector<float>.Build.Dense(((Matrix<float>)AuxDict["wih"]).ColumnCount, -1f));

                    }
                    else
                    {
                        int index_row = rand.Next(0, (int)Settings["onodes"] - 1);
                        ((Matrix<float>)AuxDict["who"]).SetRow(index_row, ((Matrix<float>)AuxDict["who"]).Row(index_row) * Random.Range(0.9f, 1.1f));
                        if (anyG(((Matrix<float>)AuxDict["who"]).Row(index_row)))
                            ((Matrix<float>)AuxDict["who"]).SetRow(index_row, Vector<float>.Build.Dense(((Matrix<float>)AuxDict["who"]).RowCount, 1f));
                        else if (anyL(((Matrix<float>)AuxDict["who"]).Row(index_row)))
                            ((Matrix<float>)AuxDict["who"]).SetRow(index_row, Vector<float>.Build.Dense(((Matrix<float>)AuxDict["who"]).RowCount, -1f));
                    }

                }
                Population.Add(new FlyingAgentData(AuxDict));
            }
        }

        public Dictionary<string, object> Settings { get; set; }
        public List<FlyingAgentData> Population { get; set; }   
        public float GenerationTime { get; set; }
    }
}

