﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MathNet.Numerics;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;

namespace Data
{
    public class FlyingAgentData
    {
        public FlyingAgentData(Dictionary<string, object> keyValuePairs)
        {
            Ang = (float)keyValuePairs["ang"];
            AngRang = (float)keyValuePairs["ang_rang"];
            VMax = (float)keyValuePairs["v_max"];
            Force = (float)keyValuePairs["force"];
            Torque = (float)keyValuePairs["torque"];
            Wih = (Matrix<float>)keyValuePairs["wih"];
            Who = (Matrix<float>)keyValuePairs["who"];
            Life = (float)keyValuePairs["life"];
            Scale = (Vector3)keyValuePairs["scale"];
            Vel = (Vector3)keyValuePairs["vel"];
            Scale2 = (Vector3)keyValuePairs["scale2"];
            Vel2 = (Vector3)keyValuePairs["vel2"];
            Evasion = (float)keyValuePairs["evasion_threshold"];
            Score = 0;
        }

        public FlyingAgentData()
        {

        }

        public float Ang { get; }
        public float AngRang { get; }
        public float VMax { get; }
        public float Force { get;}
        public float Torque { get; }
        public float Life { get; }
        public float Evasion { get; }
        public int Score { get; set; }
        public float Fitness { get; set; }
        public Matrix<float> Wih { get; }
        public Matrix<float> Who { get; }
        public Vector3 Scale { get; }
        public Vector3 Vel { get; }
        public Vector3 Scale2 { get; }
        public Vector3 Vel2 { get; }
        public float AliveTime { get; set; }
    }
}
