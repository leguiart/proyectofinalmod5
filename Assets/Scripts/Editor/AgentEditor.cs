﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Agent))]
[CanEditMultipleObjects]
public class AgentEditor : Editor
{
    SerializedProperty triggerColliderTag, health, hitDamage, updatePatrolPosition, detectionRadius, detectWithTrigger, m_screams, m_dies, m_grunts, m_attacks, m_bites, patrolRange;
    void OnEnable()
    {
        triggerColliderTag = serializedObject.FindProperty("triggerColliderTag");
        health = serializedObject.FindProperty("health");
        hitDamage = serializedObject.FindProperty("hitDamage");
        updatePatrolPosition = serializedObject.FindProperty("updatePatrolPosition");
        detectionRadius = serializedObject.FindProperty("detectionRadius");
        detectWithTrigger = serializedObject.FindProperty("detectWithTrigger");
        m_screams = serializedObject.FindProperty("m_screams");
        m_dies = serializedObject.FindProperty("m_dies");
        m_grunts = serializedObject.FindProperty("m_grunts");
        m_attacks = serializedObject.FindProperty("m_attacks");
        m_bites = serializedObject.FindProperty("m_bites");
        patrolRange = serializedObject.FindProperty("patrolRange");
    }

    public override void OnInspectorGUI()
    {
        // If we call base the default inspector will get drawn too.
        // Remove this line if you don't want that to happen.
        serializedObject.Update();
        EditorGUILayout.PropertyField(health);
        EditorGUILayout.PropertyField(hitDamage);
        
        EditorGUILayout.PropertyField(m_screams, true);
        EditorGUILayout.PropertyField(m_dies, true);
        EditorGUILayout.PropertyField(m_grunts, true);
        EditorGUILayout.PropertyField(m_attacks, true);
        EditorGUILayout.PropertyField(m_bites, true);
        EditorGUILayout.PropertyField(detectionRadius);
        EditorGUILayout.PropertyField(detectWithTrigger);

        if (detectWithTrigger.boolValue)
        {
            EditorGUILayout.PropertyField(triggerColliderTag);
        }
        else
        {           
            EditorGUILayout.PropertyField(patrolRange);
            EditorGUILayout.PropertyField(updatePatrolPosition);
        }
        serializedObject.ApplyModifiedProperties();
    }
}
