﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ManualGun))]
[CanEditMultipleObjects]
public class ManualGunEditor : Editor
{
    SerializedProperty damage, range, impactForce, impactDuration, impactVolume, impactClip, shootSounds, impactEffect, GUIimage, bullet, primary, isHeavy;

    void OnEnable()
    {
        damage = serializedObject.FindProperty("damage");
        range = serializedObject.FindProperty("range");
        impactForce = serializedObject.FindProperty("impactForce");
        impactDuration = serializedObject.FindProperty("impactDuration");
        impactVolume = serializedObject.FindProperty("impactVolume");
        impactClip = serializedObject.FindProperty("impactClip");
        shootSounds = serializedObject.FindProperty("shootSounds");
        impactEffect = serializedObject.FindProperty("impactEffect");
        GUIimage = serializedObject.FindProperty("GUIimage");
        primary = serializedObject.FindProperty("primary");
        isHeavy = serializedObject.FindProperty("isHeavy");
        bullet = serializedObject.FindProperty("bullet");
    }

    public override void OnInspectorGUI()
    {
        // If we call base the default inspector will get drawn too.
        // Remove this line if you don't want that to happen.
        serializedObject.Update();
        EditorGUILayout.PropertyField(damage);
        EditorGUILayout.PropertyField(range);
        EditorGUILayout.PropertyField(impactForce);
        EditorGUILayout.PropertyField(impactDuration);
        EditorGUILayout.PropertyField(impactVolume);
        EditorGUILayout.PropertyField(impactClip);
        EditorGUILayout.PropertyField(shootSounds, true);
        EditorGUILayout.PropertyField(impactEffect);
        EditorGUILayout.PropertyField(GUIimage);
        EditorGUILayout.PropertyField(primary);
        EditorGUILayout.PropertyField(isHeavy);
        if(isHeavy.boolValue)
        {
            EditorGUILayout.PropertyField(bullet);
        }
        serializedObject.ApplyModifiedProperties();
    }
}