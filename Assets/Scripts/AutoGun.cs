﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "NewAutoGun", menuName = "Guns/AutomaticGun")]
public class AutoGun : ScriptableObject
{
    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 15f;
    public float impactForce = 30f;
    public float impactDuration = 2f;
    public float impactVolume = 2f;
    public AudioClip impactClip;
    public AudioClip[] shootSounds;
    public GameObject impactEffect;
    public Image GUIimage;
    public bool primary;

}
