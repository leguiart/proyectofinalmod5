﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Data;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance { get { return instance; } }
    //public Player Player { get { return player; } }
    [Header("Player Settings")]
    public string playerFileName;
    public string flyingAgentsData;
    public int lives = 3;
    public float health = 100f;
    [Header("Evasion movement settings")]
    public Vector3 scale_median = new Vector3(1f, 1f, 1f), scale_std_dev = new Vector3(0.5f, 1f, 1.5f);

    public Vector3 vel_median = new Vector3(1.5f, 1f, 1f), vel_std_dev = new Vector3(0.8f, 0.5f, 1.5f);
    [Header("Genotype settings")]
    public int pop_size = 20;
    public float elitism = 0.2f, mutate = 0.3f;
    public float ang_median = 0f, ang_std_dev = 20f, ang_rang_median = 20f, ang_rang_std_dev = 15f, v_max = 5f, force_median = 100f, force_std_dev = 20f, torque_median = 13f, torque_std_dev = 5f, faLife = 100f;
    public Vector2 battleFieldX;
    public Vector2 battleFieldY;
    [Header("Control Settings")]
    public float wih_median = 0f, wih_std_dev = 1f, who_median = 0f, who_std_dev = 0.8f;
    public int hnodes = 20, onodes = 2;

    private static DataManager instance;
    private Player player;
    private FlyingAgentsData flyingAgents;
    private Dictionary<string, object> settings;

    void Awake(){
        instance = this;
        player = new Player();
        settings = new Dictionary<string, object> { { "wih_median", wih_median }, { "wih_std_dev", wih_std_dev }, { "who_median", who_median }, { "who_std_dev", who_std_dev } ,
            { "pop_size", pop_size}, { "ang_median", ang_median}, { "ang_std_dev", ang_std_dev},
            { "ang_rang_median", ang_rang_median}, { "ang_rang_std_dev", ang_rang_std_dev}, { "v_max", v_max}, {"force_median", force_median}, {"force_std_dev", force_std_dev},
            { "torque_median", torque_median}, { "torque_std_dev", torque_std_dev}, { "hnodes", hnodes}, { "onodes", onodes}, {"scale_median", scale_median},
            { "scale_std_dev", scale_std_dev}, { "vel_median", vel_median}, { "vel_std_dev", vel_std_dev}, { "life", faLife}, { "elitism", elitism}, { "mutate", mutate}
        };
    }
    
    public Player GetPlayerData(){
    //#if FILE_SYSTEM
        if(File.Exists(Application.streamingAssetsPath + "/" + playerFileName)){
            using (StreamReader sr = new StreamReader(Application.streamingAssetsPath + "/" + playerFileName))
            {
                //player = JsonUtility.FromJson<Player>(sr.ReadToEnd());
                player = JsonConvert.DeserializeObject<Player>(sr.ReadToEnd());
                //sr.ReadToEnd();
            }
        }
        else
        {
            player.lives = lives;
            player.health = health;
            player.originalHealth = health;
            player.kills = 0;
            player.waveNum = 0;
            player.musicVolume = 0f;

            player.sfxVolume = 0f;
        }
        return player;
    //#else

    //    if(PlayerPrefs.HasKey("Lives")){
    //        player.lives = PlayerPrefs.GetInt("Lives");
    //        player.kills = PlayerPrefs.GetInt("Items");
    //        player.musicVolume = PlayerPrefs.GetFloat("Music");
    //        player.sfxVolume = PlayerPrefs.GetFloat("SFX");
    //    }else{
    //        player.lives = 3;
    //        player.kills = 0;
    //        player.musicVolume = 0;
    //        player.sfxVolume = 0;
    //    }
    //    return player;
    //#endif
    }

    public void SavePlayerData(Player p){
    //#if FILE_SYSTEM
        using(StreamWriter sw = new StreamWriter(Application.streamingAssetsPath + "/" + playerFileName, false))
        {
            //sw.Write(JsonUtility.ToJson(p));
            sw.Write(JsonConvert.SerializeObject(p));
        }
    //#else
    //    PlayerPrefs.SetInt("Lives", p.lives);
    //    PlayerPrefs.SetInt("Items", p.kills);
    //    PlayerPrefs.SetFloat("Music", p.musicVolume);
    //    PlayerPrefs.SetFloat("SFX", p.sfxVolume);
    //    PlayerPrefs.Save();
    //#endif
    }

    public FlyingAgentsData GetFlyingAgentsData()
    {
        if (File.Exists(Application.streamingAssetsPath + "/" + flyingAgentsData))
        {
            using (StreamReader sr = new StreamReader(Application.streamingAssetsPath + "/" + flyingAgentsData))
            {
                //player = JsonUtility.FromJson<Player>(sr.ReadToEnd());
                flyingAgents = JsonConvert.DeserializeObject<FlyingAgentsData>(sr.ReadToEnd());
                //sr.ReadToEnd();
            }
        }
        else
        {
            flyingAgents = new FlyingAgentsData(settings);
        }
        return flyingAgents;
    }

    public void SetFlyingAgentsData(FlyingAgentsData flying)
    {
        using (StreamWriter sw = new StreamWriter(Application.streamingAssetsPath + "/" + flyingAgentsData, false))
        {
            //sw.Write(JsonUtility.ToJson(p));
            sw.Write(JsonConvert.SerializeObject(flying));
        }
    }

}
