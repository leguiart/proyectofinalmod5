﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPlayer : MonoBehaviour
{
    private float hitDamage = 50f;
    private Animator animator;
    private Agent agent;

    private void Start()
    {
        animator = GetComponentInParent<Animator>();
        agent = GetComponentInParent<Agent>();
        hitDamage = agent.hitDamage;
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.transform.tag == "Player" && (animator.GetBehaviour<StateBehaviour2>().GetOnState() || animator.GetBehaviour<StateBehaviour3>().GetOnState()))
        //    other.gameObject.GetComponent<PlayerController>().TakeDamage(hitDamage);
        //else if (other.transform.tag == "muerte" && (animator.GetBehaviour<StateBehaviour2>().GetOnState() || animator.GetBehaviour<StateBehaviour3>().GetOnState()))
        //    other.gameObject.GetComponent<PlayerController>().health = 0;
    }
}
