﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEvent : MonoBehaviour
{
    public List<GameObject> spawnGameObjects;
    private List<SpawnPoint> spawnPoints;

    private void Start()
    {
        if (spawnGameObjects != null)
        {
            spawnPoints = new List<SpawnPoint>();
            foreach (GameObject spawnGameObject in spawnGameObjects)
                spawnPoints.Add(spawnGameObject.GetComponent<SpawnPoint>());               
        }
        else
            Debug.Log("Can't find SpawnPoint objects");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
            foreach(SpawnPoint spawnPoint in spawnPoints)
                spawnPoint.ActivateSpawn();
    }
}
