﻿
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get { return instance; } }

    public AudioMixer Mixer;

    private static AudioManager instance;
    private float musicVolume;
    private float sfxVolume;
    void Awake(){
        instance = this;
    }
    void Start()
    {
        Mixer.GetFloat("MusicVolume", out musicVolume);
        Mixer.GetFloat("SFXVolume", out sfxVolume);
    }

    public void SetMusicVolume(float volume){
        Mixer.SetFloat("MusicVolume", volume);
    }

    public void SetSFXVolume(float volume){
        Mixer.SetFloat("SFXVolume", volume);
    }
}
