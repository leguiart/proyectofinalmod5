﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


namespace Assets.Scripts
{
    class RayCastShooter : MonoBehaviour, IShooter
    {
        public float range, damage, impactForce, impactVolume, ImpactDuration;
        public AudioClip impactClip;
        public GameObject impactEffect;
        public void Shoot()
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, range))
            {
                Agent agent = hit.transform.GetComponent<Agent>();
                FlyingAgent flyingAgent = hit.transform.GetComponent<FlyingAgent>();
                PlayerController playerController = hit.transform.GetComponentInParent<PlayerController>();
                if (agent != null)
                    agent.TakeDamage(damage);
                if (playerController != null)
                {
                    playerController.TakeDamage(damage);
                    GetComponentInParent<FlyingAgent>().Score++;
                }

                if (flyingAgent != null && gameObject.GetComponentInParent<PlayerController>() != null)
                    flyingAgent.TakeDamage(damage);

                if (hit.rigidbody != null)
                    hit.rigidbody.AddForce(-hit.normal * impactForce, ForceMode.Impulse);
                GameObject impact = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
                AudioSource.PlayClipAtPoint(impactClip, hit.point, impactVolume);
                Destroy(impact, ImpactDuration);
            }
        }
    }
}
