﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Price : MonoBehaviour
{
    public Vector3 vec, scale, vel;
    public float rotVel = 10f;
    private Vector3 Delta;
    private HarmonicMovement hm;

    private void Start()
    {
        hm = new HarmonicMovement(scale, vel, rotVel);
    }

    private void Update()
    {
        hm.SetSpiral();
        hm.SetPosition(this.gameObject);
        hm.SetRotation(vec, this.gameObject);
    }

    public void SetRotation(Vector3 vec, GameObject go)
    {
        go.transform.Rotate(vec * Time.deltaTime * rotVel, Space.Self);
    }

    public void SetPosition(GameObject go)
    {
        go.transform.position += new Vector3(Delta.x * scale.x, Delta.y * scale.y, Delta.z * scale.z);       //Scale our height by a factor 
        go.transform.Translate(Delta.x * scale.x, Delta.y * scale.y, Delta.z * scale.z, Space.World);
    }

    //We need to add a rigid body component on the editor in order to make the pick up objects dynamic, for performance reasons (static colliders move, dynamic colliders don't)
    //In order for this to work, we need to either deactivate gravity for the objects, though they will still be affected by physics forces
    //Or to make them kinematic, such that unity will ignore any physics related to the rigid body component and just make them move according to 
    //the effects from under
    public void SetFrenet()
    {
        Delta = new Vector3(Mathf.Sin(Time.time + Time.deltaTime) - Mathf.Sin(Time.time), Time.time, Mathf.Cos(Time.time) - Mathf.Cos(Time.time + Time.deltaTime));
    }

    public void SetLine()
    {
        Delta = new Vector3(Time.time + Time.deltaTime, Time.time + Time.deltaTime, Time.time + Time.deltaTime);
    }

    public void SetSpiral()
    {
        Delta = new Vector3(Mathf.Cos(Time.time + Time.deltaTime) - Mathf.Cos(Time.time), Mathf.Sin(Time.time + Time.deltaTime) - Mathf.Sin(Time.time), Mathf.Sin(Time.time) - Mathf.Sin(Time.time + Time.deltaTime));
    }

    public void SetHarmonicCurve()
    {
        if (vel.x == vel.z)
        {
            Delta = new Vector3(Mathf.Sin(Time.time * vel.x + Time.deltaTime) - Mathf.Sin(Time.time), Mathf.Sin(Time.time * vel.y + Time.deltaTime) - Mathf.Sin(Time.time), Mathf.Cos(Time.time) - Mathf.Cos(Time.time * vel.z + Time.time));
        }
        else
        {
            Delta = new Vector3(Mathf.Cos(Time.time * vel.x + Time.deltaTime) - Mathf.Cos(Time.time), Mathf.Sin(Time.time * vel.y + Time.deltaTime) - Mathf.Sin(Time.time), Mathf.Sin(Time.time) - Mathf.Sin(Time.time * vel.z + Time.deltaTime));
        }
    }

    protected void SetCurveDelta()
    {
    }
}
