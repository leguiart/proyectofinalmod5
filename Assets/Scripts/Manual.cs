﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manual : Gun
{
    public ManualGun manualGun;
    private bool isHeavy;
    // Start is called before the first frame update
    void Start()
    {
        isHeavy = manualGun.isHeavy;
        this.Initialize(false);
        if (shooter != null)
        {
            if(isHeavy){
                shooter.bullet = manualGun.bullet;
                shooter.bullet.GetComponent<Bullet>().impactEffect = manualGun.impactEffect;
                shooter.bullet.GetComponent<Bullet>().damage = manualGun.damage;
                shooter.bullet.GetComponent<Bullet>().impactDuration = manualGun.impactDuration;
                shooter.bullet.GetComponent<Bullet>().impactClip = manualGun.impactClip;
                shooter.bullet.GetComponent<Bullet>().impactVolume = manualGun.impactVolume;
                shooter.force = manualGun.impactForce;
            }else{
                impactEffect = manualGun.impactEffect;
                damage = manualGun.damage;
                impactForce = manualGun.impactForce;
                range = manualGun.range;
            }

        }
        else
        {
            Debug.Log("Can't find Shooter.cs script");
        }
        shootSounds = manualGun.shootSounds;
        primary = manualGun.primary;
        impactClip = manualGun.impactClip;

    }
    

    // FixedUpdate for rigid body simulation
    void FixedUpdate()
    {
        if (shooting)
        {
            PlayShootSound();
            particleSystem.Play();
            this.Shoot();
        }
        this.RaycastHit();
    }

    protected new void Shoot()
    {
        if(isHeavy)
            shooter.ShootSingleBullet();
        else
            base.Shoot();
    }

}
