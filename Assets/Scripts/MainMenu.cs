﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class MainMenu : MonoBehaviour {
    public string playerFileName, flyingAgentsData;
    public GameObject lifeBar, loadingPanel, mainMenuPanel;
    private AsyncOperation sceneLoader;
    private LifeBarController lifeBarController;
    private bool loading;

    void Start()
    {
        lifeBarController = lifeBar.GetComponent<LifeBarController>();
        CleanPanels();
        mainMenuPanel.SetActive(true);
    }

    void CleanPanels()
    {
        loadingPanel.SetActive(false);
        mainMenuPanel.SetActive(false);
    }

    public void PlayGame()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
        sceneLoader = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
        CleanPanels();
        loadingPanel.SetActive(true);
        loading = true;
    }

    public void NewGame()
    {
        if (File.Exists(Application.streamingAssetsPath + "/" + playerFileName))
        {
            File.Delete(Application.streamingAssetsPath + "/" + playerFileName);
        }
        if (File.Exists(Application.streamingAssetsPath + "/" + flyingAgentsData))
        {
            File.Delete(Application.streamingAssetsPath + "/" + flyingAgentsData);
        }
        PlayGame();
    }

    private void Update()
    {
        if (loading)
        {
            lifeBarController.SetLifeLevel(sceneLoader.progress);

        }
        else
        {
            lifeBarController.SetLifeLevel(0f);
        }
    }

    public void ShowOptions()
    {

    }

    public void QuitGame()
    {
        Application.Quit();

    }
}
