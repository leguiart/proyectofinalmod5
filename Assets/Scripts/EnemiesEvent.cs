﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesEvent : MonoBehaviour {
    private bool activeEnemies = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            this.activeEnemies = true;
    }

    public bool GetEnemiesStatus()
    {
        return this.activeEnemies;
    }
}
