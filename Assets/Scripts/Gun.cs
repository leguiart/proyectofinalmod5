﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour {
    
    public bool Shooting{get{return shooting;}set{shooting = value;}}
    public float Range { get { return range; } }
    protected float ImpactDuration;
    protected float damage;
    protected float range;
    protected float fireRate;
    protected float impactForce;
    protected float impactVolume;
    protected AudioClip impactClip;
    protected AudioClip[] shootSounds;
    protected GameObject impactEffect;
    protected bool primary, inInventory, isEquipped, shooting, multiple;
    protected int layerMask, layerMaskEvasion;
    protected AudioSource source;
    protected new ParticleSystem particleSystem;
    protected ParticleSystem[] particleSystems;
    protected Shooter shooter;
    
    protected float nextTimeToFire = 0f;
    protected float delay = 0f;
    protected float delayRate = 0.55f;
    public SpriteRenderer GUIimage;

    protected void PlayShootSound() {
        // pick & play a random shoot sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1,shootSounds.Length);
        source.clip = shootSounds[n];
        source.PlayOneShot(source.clip, Random.Range(0.7f, 1.0f));
        // move picked sound to index 0 so it's not picked next time
        shootSounds[n] = shootSounds[0];
        shootSounds[0] = source.clip;
    }

    public void Shoot()
    {
        if(this.multiple){
            foreach(ParticleSystem ps in particleSystems){
                ps.Play();
                //Debug.Log("Play Particle System");
            }

        }else{
            particleSystem.Play();
        }

        
        RaycastHit hit;
        if(Physics.Raycast(shooter.transform.position, shooter.transform.forward, out hit, range))
        {
            Agent agent = hit.transform.GetComponent<Agent>();
            FlyingAgent flyingAgent = hit.transform.GetComponent<FlyingAgent>();
            PlayerController playerController = hit.transform.GetComponentInParent<PlayerController>();
            if (agent != null)
                agent.TakeDamage(damage);
            if (playerController != null) {
                playerController.TakeDamage(damage);
                GetComponentInParent<FlyingAgent>().Score++;
            }

            if (flyingAgent != null && gameObject.GetComponentInParent<PlayerController>() != null)
                flyingAgent.TakeDamage(damage);

            if (hit.rigidbody != null)
                hit.rigidbody.AddForce(-hit.normal * impactForce, ForceMode.Impulse);
            GameObject impact = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            AudioSource.PlayClipAtPoint(impactClip, hit.point, impactVolume);
            Destroy(impact, ImpactDuration);
        }
    }

    protected void Initialize(bool multiple){
        if(multiple){
            particleSystems = GetComponentsInChildren<ParticleSystem>();
        }else{
            particleSystem = GetComponentInChildren<ParticleSystem>();
        }
        this.multiple = multiple;
        source = GetComponent<AudioSource>();
        layerMask = LayerMask.GetMask("Enemies");
        layerMaskEvasion = LayerMask.GetMask("EnemiesEvasion");
        shooting = false;
        shooter = GetComponentInChildren<Shooter>();

        //GUIimage = GetComponentInChildren<SpriteRenderer>();

    } 

    protected void RaycastHit(){
        RaycastHit hit;
        if (GUIimage != null)
        {
            Debug.DrawRay(shooter.gameObject.transform.position, shooter.gameObject.transform.forward * range, Color.blue);

            if (Physics.Raycast(shooter.gameObject.transform.position, shooter.gameObject.transform.forward, out hit, range, layerMask))
                GUIimage.color = Color.red;
            else
                GUIimage.color = Color.green;

        }
        if(GetComponentInParent<FlyingAgent>() == null)
            if (Physics.Raycast(shooter.gameObject.transform.position, shooter.gameObject.transform.forward, out hit, range, layerMaskEvasion))
                hit.transform.GetComponent<FlyingAgent>().SetEvade();//Debug.Log("Hitting Evasion collider");





    }
}
