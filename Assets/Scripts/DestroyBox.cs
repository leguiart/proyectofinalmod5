﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The following script's purpose is that of destroying any hazard or proyectile which is out of some render volume
/// By using a bounding box trigger collider
/// </summary>

public class DestroyBox : MonoBehaviour
{
    //Just use a Unity API built-in function OnTriggerExit, which function is that of upon exiting a trigger volume
    //execute the code inside it
    private void OnTriggerExit(Collider other)
    {
        //In this case destroy any such object that exits the trigger collider volume
        //other.gameObject.SetActive(false);
        if(other.gameObject.GetComponentInParent<FlyingAgent>()!=null)
            ObjectPool.instance.PoolGameObject(other.gameObject.GetComponentInParent<FlyingAgent>().gameObject);
    }
}