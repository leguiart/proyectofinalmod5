﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject enemy;
    public Vector3 spawnValues;

    public int hazardCount = 1;
    public float spawnWait = 1f, waveWait = 1f, startWait = 1f;

    public void Spawn()
    {
        Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);//Spawn hazard on random x position in a given range, and given y and z values
        Quaternion spawnRotation = Quaternion.identity;//Set the rotation to the identity matrix (no rotation), since this will be given by the RandomRotator class
        Instantiate(enemy, gameObject.transform.position + spawnPosition, spawnRotation);//Produce an instance of the game object class hazard is associated to
    }

    //Method for Spawning waves of hazards
    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);//Wait a certain time until the first wave appears, upon starting the game
        //while (!gameOver)
        //{
        for (int i = 0; i < hazardCount; i++)
        {
            Spawn();
            //Debug.Log("Spawning: " + spawnPoints.Count);
            //These functions work with special yield statements which return the code execution out of the function
            yield return new WaitForSeconds(spawnWait);//Wait certain spawn wait time until the next hazard spawn
        }
        //yield return new WaitForSeconds(waveWait);//Wait a certain time until the next wave
        StopCoroutine(SpawnWaves());
        //}
    }

    public void ActivateSpawn()
    {
        StartCoroutine(SpawnWaves());
    }
}
