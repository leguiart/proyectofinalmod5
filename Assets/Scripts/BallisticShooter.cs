﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    class BallisticShooter: MonoBehaviour, IShooter
    {
        [HideInInspector] public GameObject bullet;
        [HideInInspector] public float force;
        private GameObject tmpBullet;
        public void Shoot()
        {
            tmpBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            tmpBullet.transform.forward = transform.forward;
            tmpBullet.GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Impulse);
        }
    }
}
