﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    
    [HideInInspector] public GameObject bullet;
    [HideInInspector] public float force;

    private GameObject tmpBullet;
    // Update is called once per frame
    public void ShootSingleBullet()
    {
        tmpBullet = Instantiate(bullet, transform.position, Quaternion.identity);
        tmpBullet.transform.forward = transform.forward;
        tmpBullet.GetComponent<Rigidbody>().AddForce(transform.forward * force, ForceMode.Impulse);
    }
}
