﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using TMPro;
using System;
using Data;
using System.Linq;

[SerializeField]
public enum GameState { PLAYING, PAUSE }
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    public FlyingAgentsData FlyingAgents { get; private set; }
    public Player Player { get; private set; }
    public bool GameOver { get; private set; }
    public float ElapsedTime { get; private set; }
    public bool DisableEnemies = false;
    public List<GameObject> spawnGameObjects;
    public List<Transform> otherAgents;
    public UnityEvent OnPlay;
    public float distance_influence = 40f, k_rej = 60f, k_Att = 50f;
    public float waveDuration = 5f;
    public Vector2 battleFieldX;
    public Vector2 battleFieldY;
    public Vector2 battleFieldZ;
    public bool debug = false;
    private float nexTime = 0f;
    private int kills = 0;
    private Rigidbody rb;
    private List<SpawnPoint> spawnPoints;
    private List<GameObject> flyingAgents;
    private PlayerController playerController;
    private GameState currentState;
    private int generation = 1;

    void Awake()
    {
        Instance = this;
        Player = new Player();
        FlyingAgents = new FlyingAgentsData();
        Player = DataManager.Instance.GetPlayerData();
        if (!DisableEnemies)
        {
            FlyingAgents = DataManager.Instance.GetFlyingAgentsData();
            FlyingAgents.Populate();
            flyingAgents = ObjectPool.instance.GetAllGameObjectsOfType("FlyingEnemy").ToList();
            otherAgents = flyingAgents.ConvertAll(x => x.gameObject.transform);

            int index = 0;
            foreach (FlyingAgentData flyingAgentData in FlyingAgents.Population)
            {
                //flyingAgents[index].GetComponent<FlyingAgent>().AgentData = new FlyingAgentData();
                var flyingAgent = flyingAgents[index].GetComponent<FlyingAgent>();

                flyingAgent.AgentData = flyingAgentData;
                flyingAgent.AgentData.AliveTime = 0f;
                flyingAgent.debug = debug;
                flyingAgent.battleFieldX = battleFieldX;
                flyingAgent.battleFieldY = battleFieldY;
                flyingAgent.battleFieldZ = battleFieldZ;
                flyingAgent.distance_influence = distance_influence;
                flyingAgent.k_Att = k_Att;
                flyingAgent.k_rej = k_rej;
                //List<GameObject> otherAgents = flyingAgents.Where(x => x != flyingAgent).ToList();//Get necessary initial reference
                //flyingAgent.GetComponent<FlyingAgent>().otherAgents = ObjectPool.instance.transformPool["FlyingEnemy"].Where(x => x != flyingAgent.transform).ToList();
                ++index;
            }
        }


    }

    void Start()
    {
        GameOver = false;

        GameObject playerObject = GameObject.FindWithTag("Player");
        UIManager.Instance.SetHealthValue(Player.health / Player.originalHealth);
        UIManager.Instance.SetRestartText("Generation: " + generation);
        if (playerObject != null)
        {
            rb = playerObject.GetComponent<Rigidbody>();
            playerController = playerObject.GetComponent<PlayerController>();
        }           
        else
            Debug.Log("Can't find player component");

        Play();
        if (spawnGameObjects != null)
        {
            spawnPoints = new List<SpawnPoint>();
            foreach (GameObject spawnGameObject in spawnGameObjects)
                spawnPoints.Add(spawnGameObject.GetComponent<SpawnPoint>());
            ElapsedTime = 0f;
        }
        else
            Debug.Log("Can't find SpawnPoint objects");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Pause();

        if (GameOver)
        {
            nexTime += Time.time;
            if (nexTime >= 2f)
            {
                UIManager.Instance.SetRestartPanel();
                playerController.SetMouseState(true, CursorLockMode.None);
                nexTime = 0f;
            }
        }
        if (!DisableEnemies)
        {
            if (ElapsedTime >= waveDuration)
            {
                flyingAgents = ObjectPool.instance.GetAllGameObjectsOfType("FlyingEnemy").ToList();
                foreach (GameObject agent in flyingAgents)
                {
                    FlyingAgent flyingAgent = agent.GetComponent<FlyingAgent>();
                    if (flyingAgent.gameObject.activeSelf)
                    {
                        flyingAgent.SetDead(true);
                        flyingAgent.DeadAction();
                    }
                }
                ElapsedTime = 0f;
            }
            if (ObjectPool.instance.AllPooled("FlyingEnemy"))
            {
                if (ElapsedTime == 0f)
                    ElapsedTime = waveDuration;
                flyingAgents = ObjectPool.instance.GetAllGameObjectsOfType("FlyingEnemy").ToList();
                int index = 0;
                FlyingAgents.GenerationTime = ElapsedTime;
                foreach (GameObject agent in flyingAgents)
                {
                    FlyingAgent flyingAgent = agent.GetComponent<FlyingAgent>();
                    FlyingAgents.Population[index] = flyingAgent.AgentData;
                    ++index;
                }

                FlyingAgents.Evolve(generation++);
                UIManager.Instance.SetRestartText("Generation: " + generation);
                index = 0;
                foreach (FlyingAgentData flyingAgentData in FlyingAgents.Population)
                {
                    FlyingAgent flyingAgent = flyingAgents[index].GetComponent<FlyingAgent>();
                    flyingAgent.AgentData = flyingAgentData;
                    flyingAgent.SetInitialParameters();

                    ++index;
                }
            }
            ElapsedTime += Time.deltaTime / 60f;
        }

    }

    public void ChangeState(GameState newState)
    {
        currentState = newState;
        switch (newState)
        {
            case GameState.PLAYING:
                UIManager.Instance.SetKills(Player.kills);
                UIManager.Instance.SetLives(Player.lives);
                playerController.SetMouseState(false, CursorLockMode.Locked);
                Time.timeScale = 1f;
                OnPlay.Invoke();
                break;
            case GameState.PAUSE:
                playerController.SetMouseState(true, CursorLockMode.None);
                UIManager.Instance.ShowPause();
                Time.timeScale = 0f;
                break;
        }
    }

    public void Play()
    {
        ChangeState(GameState.PLAYING);
    }

    public void Pause()
    {
        ChangeState(GameState.PAUSE);
    }

    public void SetLives()
    {
        Player.lives--;
        UIManager.Instance.SetLives(Player.lives);
    }

    public void RewardLives()
    {
        Player.lives++;
        UIManager.Instance.SetLives(Player.lives);
    }

    public void RewardLife()
    {

    }

    public void SetKills()
    {
        Player.kills++;
        UIManager.Instance.SetKills(Player.kills);

    }



    public void SetHealthValue(float damage)
    {
        Player.health -= damage;
        UIManager.Instance.SetHealthValue(Player.health / Player.originalHealth);
    }

    public void ResetHealth()
    {
        Player.health = Player.originalHealth;
        UIManager.Instance.SetHealthValue(Player.health / Player.originalHealth);
    }

    public void ResetKills()
    {
        Player.kills = 0;
        UIManager.Instance.SetKills(Player.kills);
    }

    public void ResetLifes()
    {
        Player.lives = 3;
        UIManager.Instance.SetLives(Player.lives);
    }

    public void UpdateGameOver()
    {
        GameOver = true;
        playerController.SetMouseState(true, CursorLockMode.None);
        UIManager.Instance.SetGameOverText("Wasted!");
    }
}
